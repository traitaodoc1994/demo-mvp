package com.example.mvp.demomvp.ui.main;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mvp.demomvp.R;
import com.example.mvp.demomvp.base.BaseActivity;
import com.example.mvp.demomvp.model.movie.Movie;
import com.squareup.picasso.Picasso;

import static com.example.mvp.demomvp.utils.Contants.API.API_KEY;


public class MainActivity extends BaseActivity implements IMainView{

    private TextView mTvMovieName;
    private TextView mTvYear;
    private TextView mTvId;
    private TextView mTvType;
    private TextView mTvDescription;
    private ImageView mImvPoster;
    private EditText mEdtSearch;
    private Button mBtnGetData;
    MainPst mainPst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        mainPst = new MainPst(this);

        mBtnGetData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainPst.getDataMovie(MainActivity.this,API_KEY,mEdtSearch.getText().toString().trim());
                showProcessDialog();
            }
        });

    }

    private void initView(){
        mTvMovieName = findViewById(R.id.text_movie_name);
        mTvDescription = findViewById(R.id.text_description);
        mTvId = findViewById(R.id.text_id);
        mTvType = findViewById(R.id.text_type);
        mTvYear = findViewById(R.id.text_year);
        mImvPoster = findViewById(R.id.image_poster);
        mEdtSearch = findViewById(R.id.edit_text_search);
        mBtnGetData = findViewById(R.id.button_get_data);
    }

    @Override
    public void onRequestSuccess(Movie movie) {
        mTvMovieName.setText(movie.getTitle());
        mTvYear.setText(movie.getYear());
        mTvType.setText(movie.getType());
        mTvId.setText(movie.getImdbID());
        mTvDescription.setText(movie.getPlot());
        Picasso.with(this).load(movie.getPoster()).into(mImvPoster);
        hideProcessDialog();
    }

    @Override
    public void onRequestError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        hideProcessDialog();
    }
}
