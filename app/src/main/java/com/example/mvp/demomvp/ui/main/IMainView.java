package com.example.mvp.demomvp.ui.main;

import com.example.mvp.demomvp.model.movie.Movie;

/**
 * Created by MSI GL627RD on 1/11/2018.
 */

public interface IMainView {
    void onRequestSuccess(Movie movie);
    void onRequestError(String msg);
}
