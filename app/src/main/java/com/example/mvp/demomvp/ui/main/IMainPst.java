package com.example.mvp.demomvp.ui.main;

import android.content.Context;

/**
 * Created by MSI GL627RD on 1/11/2018.
 */

public interface IMainPst {

    void getDataMovie(Context context,String apiKey, String title);

}
