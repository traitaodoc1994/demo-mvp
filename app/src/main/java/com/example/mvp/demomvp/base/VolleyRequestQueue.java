package com.example.mvp.demomvp.base;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by MSI GL627RD on 1/12/2018.
 */

public class VolleyRequestQueue {
        private static VolleyRequestQueue mInstance;
        private RequestQueue mRequestQueue;
        private static Context mCtx;

        private VolleyRequestQueue(Context context) {
            mCtx = context;
            mRequestQueue = getRequestQueue();
        }

        public static synchronized VolleyRequestQueue getInstance(Context context) {
            if (mInstance == null) {
                mInstance = new VolleyRequestQueue(context);
            }
            return mInstance;
        }

        public RequestQueue getRequestQueue() {
            if (mRequestQueue == null) {
                mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
            }
            return mRequestQueue;
        }

        public <T> void addToRequestQueue(Request<T> req) {
            getRequestQueue().add(req);
        }

}
