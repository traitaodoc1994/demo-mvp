package com.example.mvp.demomvp.base;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.mvp.demomvp.R;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by MSI GL627RD on 1/11/2018.
 */

public class BaseActivity extends AppCompatActivity {

    ACProgressFlower dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        dialog = new ACProgressFlower.Builder(this)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .text(getString(R.string.msg_loading))
                .fadeColor(Color.DKGRAY).build();
        super.onCreate(savedInstanceState);
    }

    protected void showProcessDialog(){
        if(!dialog.isShowing()){
            dialog.show();
        }
    }

    protected void hideProcessDialog(){
        if(dialog.isShowing()){
            dialog.dismiss();
        }
    }


}
