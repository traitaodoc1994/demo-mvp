package com.example.mvp.demomvp.ui.main;

import android.content.Context;

import com.android.volley.VolleyError;
import com.example.mvp.demomvp.base.VolleyHelper;
import com.example.mvp.demomvp.model.movie.Movie;
import com.example.mvp.demomvp.utils.VolleyUtils;

import java.util.HashMap;

import static com.example.mvp.demomvp.utils.Contants.API.API_KEY;
import static com.example.mvp.demomvp.utils.Contants.API.HOST;

/**
 * Created by MSI GL627RD on 1/11/2018.
 */

public class MainPst implements IMainPst{

    IMainView iMainView;

    public MainPst(IMainView listener) {
        this.iMainView = listener;
    }

    @Override
    public void getDataMovie(Context context,String apiKey, String title) {
        HashMap<String,String> params = new HashMap<>();
        params.put("apikey",API_KEY);
        params.put("t",title);
        String url = VolleyUtils.makeUrl(HOST,params) ;
        new VolleyHelper<Movie>().get(context, url, null, Movie.class, new VolleyHelper.IVolleyResponse<Movie>() {
            @Override
            public void onSuccess(Movie response) {
                iMainView.onRequestSuccess(response);
            }

            @Override
            public void onError(VolleyError error) {
                String msg = "Request Error";
                iMainView.onRequestError(msg);
            }
        });
    }
}
